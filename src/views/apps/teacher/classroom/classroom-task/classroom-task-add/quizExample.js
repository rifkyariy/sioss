export default [
  {
    questionNumber: 1,
    questionId: 1,
    question: 'Secara fisiografis, wilayah laut Indonesia dibagi menjadi tiga wilayah yaitu Paparan Sunda, Paparan Sahul, dan zona transisi. Paparan Sunda terletak di bagian barat Indonesia dengan ciri … dan meliputi daerah perairan … ',
    options: [
      {
        id: 1,
        text: 'Terdapat palung; Laut Banda dan Laut Aru',
      },
      {
        id: 2,
        text: 'Laut dalam; Laut Jawa dan Selat Malaka',
      },
      {
        id: 3,
        text: 'Laut dangkal; Laut Jawa dan Laut Banda',
      },
      {
        id: 4,
        text: 'Terdapat palung; Laut Jawa dan Laut Cina Selatan',
      },
      {
        id: 5,
        text: 'Laut dangkal; Selat Malaka dan Laut Cina Selatan',
      },
    ],
    answer: null,
  },
  {
    questionNumber: 2,
    questionId: 2,
    question: 'Secara etimologi kata Sejarah berasal dari Bahasa Arab Syajaratun yang berarti pohon. Adapun pengertian sejarah secara isitilah dijelaskan dalam pernyataan berikut ini, kecuali … ',
    options: [
      {
        id: 1,
        text: 'Ilmu yang mempelajari peristiwa-peristiwa pada masa lampau',
      },
      {
        id: 2,
        text: 'Sejumlah perubahan/kejadian dan peristiwa-peristiwa di sekitar kita',
      },
      {
        id: 3,
        text: 'Kisah-kisah pada masa lampau yang tidak terkait dengan kehidupan masa kini',
      },
      {
        id: 4,
        text: 'Peristiwa – peristiwa yang terjadi pada masa lampau dalam kehidupan manusia',
      },
      {
        id: 5,
        text: 'Catatan-catatan tentang peristiwa masa lampau yang disusun berdasarkan peninggalan- peninggalannya',
      },
    ],
    answer: null,
  },
  {
    questionNumber: 3,
    questionId: 3,
    question: 'Berdasarkan tingkat penyampaiannya, sumber sejarah dibagi menjadi sumber primer, sekunder dan tersier. Adapun sumber primer adalah … ',
    options: [
      {
        id: 1,
        text: 'Informasi yang diperoleh dari prantara',
      },
      {
        id: 2,
        text: 'Informasi yang dituturkan oleh pihak ketiga',
      },
      {
        id: 3,
        text: 'Informasi yang diperoleh dari pelaku atau saksi sejarah',
      },
      {
        id: 4,
        text: 'Informasi yang diperoleh dari sumber rekaman',
      },
      {
        id: 5,
        text: 'Informasi yang diperoleh dari benda sejarah',
      },
    ],
    answer: null,
  },
  {
    questionNumber: 4,
    questionId: 4,
    question: 'Contoh periodisasi peristiwa sejarah Indonesia yang tersusun secara kronologis adalah … ',
    options: [
      {
        id: 1,
        text: 'Masa Kerajaan Hindu-Buddha – Masa Kerajaan Islam – Masa pendudukan Jepang – Masa kemerdekaan',
      },
      {
        id: 2,
        text: 'Masa Kerajaan Islam – Masa Kerajaan Hindu-Buddha – Masa Kolonial Belanda – Masa pendudukan Jepang',
      },
      {
        id: 3,
        text: 'Masa kemerdekaan – Masa pendudukan Jepang – Masa Kerajaan Hindu-Buddha – Masa Kerajaan Islam – Masa Kolonial Belanda',
      },
      {
        id: 4,
        text: 'Masa pendudukan Jepang – Masa Kolonial Belanda – Masa Kerajaan Islam – Masa Kerajaan Hindu-Buddha – Masa kemerdekaan',
      },
      {
        id: 5,
        text: 'Masa Kerajaan Hindu-Buddha – Masa Kerajaan Islam – Masa Kolonial Belanda – Masa pendudukan Jepang – Masa kemerdekaan',
      },
    ],
    answer: null,
  },
]

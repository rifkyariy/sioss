export default [
  // Student
  // Dashboard
  {
    path: '/student/dashboard',
    name: 'student-dashboard',
    component: () => import('@/views/apps/dashboard/Dashboard.vue'),
    meta: {
      role: 'student',
    },
  },

  // Schedule
  {
    path: '/student/schedule',
    name: 'student-schedule',
    component: () => import('@/views/apps/schedule/Schedule.vue'),
    meta: {
      role: 'student',
      pageTitle: 'Jadwal',
      breadcrumb: [
        {
          text: 'Jadwal',
          active: true,
        },
      ],
    },
  },

  // Classroom
  {
    path: '/student/classroom',
    name: 'student-classroom',
    component: () => import('@/views/apps/classroom/Classroom.vue'),
    meta: {
      role: 'student',
      contentClass: 'classroom-menu',
      pageTitle: 'Kelas',
      breadcrumb: [
        {
          text: 'Kelas',
          active: true,
        },
      ],
    },
  },
  {
    path: '/student/classroom/:id',
    name: 'student-classroom-detail',
    component: () => import('@/views/apps/classroom/classroom-detail/ClassroomDetail.vue'),
    meta: {
      role: 'student',
      contentClass: 'classroom-menu',
      navActiveLink: 'student-classroom',
      pageTitle: 'Kelas',
      back: 'classroom',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          active: true,
        },
      ],
    },
  },
  {
    path: '/student/classroom/:id/content/:contentId/material',
    name: 'student-classroom-detail-material',
    component: () => import('@/views/apps/classroom/classroom-detail/ClassroomMaterial.vue'),
    meta: {
      role: 'student',
      contentClass: 'classroom-menu',
      navActiveLink: 'student-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Materi',
          active: true,
        },
      ],
    },
  },
  {
    path: '/student/classroom/:id/content/:contentId/task',
    name: 'student-classroom-detail-task',
    component: () => import('@/views/apps/classroom/classroom-detail/classroom-task/ClassroomTask.vue'),
    meta: {
      role: 'student',
      contentClass: 'classroom-menu',
      navActiveLink: 'student-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Tugas',
          active: true,
        },
      ],
    },
  },
  {
    path: '/student/classroom/:id/content/:contentId/task/:taskId/submission',
    name: 'student-classroom-detail-task-submission',
    component: () => import('@/views/apps/classroom/classroom-detail/classroom-task/ClassroomTaskSubmission.vue'),
    meta: {
      role: 'student',
      contentClass: 'classroom-menu',
      navActiveLink: 'student-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail-task',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Tugas',
          to: 'classroom-detail-task',
        },
        {
          text: 'Submission',
          active: true,
        },
      ],
    },
  },

  // Exam
  {
    path: '/student/exam',
    name: 'student-exam',
    component: () => import('@/views/apps/exam/Exam.vue'),
    meta: {
      role: 'student',
      contentClass: 'classroom-menu',
      pageTitle: 'Ujian',
      breadcrumb: [
        {
          text: 'Ujian',
          active: true,
        },
      ],
    },
  },
  {
    path: '/student/exam/:id',
    name: 'student-exam-detail',
    component: () => import('@/views/apps/exam/exam-detail/ExamDetail.vue'),
    meta: {
      role: 'student',
      contentClass: 'exam-menu',
      navActiveLink: 'exam',
      pageTitle: 'Ujian',
      breadcrumb: [
        {
          text: 'Ujian',
          to: 'exam',
        },
        {
          text: 'Detail',
          active: true,
        },
      ],
    },
  },
  {
    path: '/student/exam/:id/start',
    name: 'student-exam-session-start',
    component: () => import('@/views/apps/exam/exam-detail/ExamSession.vue'),
    meta: {
      role: 'student',
      contentClass: 'exam-menu',
      navActiveLink: 'exam',
      pageTitle: 'Ujian',
      breadcrumb: [
        {
          text: 'Ujian',
          to: 'exam',
        },
        {
          text: 'Detail',
        },
        {
          text: 'Sesi Ujian',
          active: true,
        },
      ],
    },
  },
]

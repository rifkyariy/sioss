export default [
  // Teacher
  // Dashboard
  {
    path: '/teacher/dashboard',
    name: 'teacher-dashboard',
    component: () => import('@/views/apps/teacher/dashboard/Dashboard.vue'),
    meta: {
      role: 'teacher',
    },
  },

  // Classroom
  {
    path: '/teacher/classroom',
    name: 'teacher-classroom',
    component: () => import('@/views/apps/teacher/classroom/Classroom.vue'),
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      pageTitle: 'Kelas',
      breadcrumb: [
        {
          text: 'Kelas',
          active: true,
        },
      ],
    },
  },
  {
    path: '/teacher/classroom/add', // Add classroom
    name: 'teacher-classroom-add',
    component: () => import('@/views/apps/teacher/classroom/classroom-add/ClassroomAdd.vue'),
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      pageTitle: 'Kelas',
      back: 'classroom',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Tambah',
          active: true,
        },
      ],
    },
  },
  {
    path: '/teacher/classroom/:id',
    name: 'teacher-classroom-detail',
    component: () => import('@/views/apps/teacher/classroom/classroom-detail/ClassroomDetail.vue'),
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      navActiveLink: 'teacher-classroom',
      pageTitle: 'Kelas',
      back: 'classroom',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          active: true,
        },
      ],
    },
  },

  // Classroom Material
  {
    path: '/teacher/classroom/:id/content/:contentId/material',
    name: 'teacher-classroom-detail-material',
    component: () => import('@/views/apps/teacher/classroom/classroom-material/ClassroomMaterial.vue'),
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      navActiveLink: 'teacher-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Materi',
          active: true,
        },
      ],
    },
  },
  {
    path: '/teacher/classroom/:id/content/:contentId/material/add',
    name: 'teacher-classroom-detail-material-add',
    component: () => import('@/views/apps/teacher/classroom/classroom-material/classroom-material-add/ClassroomMaterialAdd.vue'),
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      navActiveLink: 'teacher-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail-material',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Materi',
          to: 'classroom-detail-material',
        },
        {
          text: 'Tambah',
          active: true,
        },
      ],
    },
  },

  // Classroom Task
  {
    path: '/teacher/classroom/:id/content/:contentId/task',
    name: 'teacher-classroom-detail-task',
    component: () => import('@/views/apps/teacher/classroom/classroom-task/ClassroomTask.vue'),
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      navActiveLink: 'teacher-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Tugas',
          active: true,
        },
      ],
    },
  },
  {
    path: '/teacher/classroom/:id/content/:contentId/task/add',
    name: 'teacher-classroom-detail-task-add',
    component: () => import('@/views/apps/teacher/classroom/classroom-task/classroom-task-add/ClassroomTaskAdd.vue'),
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      navActiveLink: 'teacher-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail-task',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Tugas',
          to: 'classroom-detail-task',
        },
        {
          text: 'Tambah',
          active: true,
        },
      ],
    },
  },
  {
    path: '/teacher/classroom/:id/content/:contentId/task/:taskId/generate/quiz',
    name: 'teacher-classroom-detail-task-generate-quiz',
    component: () => import('@/views/apps/teacher/classroom/classroom-task/classroom-task-add/GenerateQuiz.vue'),
    props: true,
    meta: {
      role: 'teacher',
      contentClass: 'teacher-classroom-menu',
      navActiveLink: 'teacher-classroom',
      pageTitle: 'Kelas',
      back: 'classroom-detail-task',
      breadcrumb: [
        {
          text: 'Kelas',
          to: 'classroom',
        },
        {
          text: 'Detail',
          to: 'classroom-detail',
        },
        {
          text: 'Tugas',
          to: 'classroom-detail-task',
        },
        {
          text: 'Tambah Quiz',
          active: true,
        },
      ],
    },
  },
]

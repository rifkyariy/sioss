export default {
  superadmin: [
    {
      header: 'Akses Menu',
    },
    {
      title: 'Dashboard',
      icon: 'ti-home',
      route: 'superadmin-dashboard',
    },
    {
      title: 'Tahun Ajaran',
      icon: 'ti-calendar-stats',
      route: 'superadmin-school-year',
    },
    {
      title: 'Jadwal',
      icon: 'ti-calendar-event',
      route: 'superadmin-schedule',
    },
    {
      title: 'Kelas',
      icon: 'ti-school',
      route: 'superadmin-classroom',
    },
    {
      title: 'Ujian',
      icon: 'ti-file-text',
      route: 'superadmin-exam',
    },
    {
      title: 'Hasil Belajar',
      icon: 'ti-stars',
      route: 'superadmin-report',
    },
  ],
  teacher: [
    {
      header: 'Akses Menu',
    },
    {
      title: 'Dashboard',
      icon: 'ti-home',
      route: 'teacher-dashboard',
    },
    {
      title: 'Kelas',
      icon: 'ti-school',
      children: [
        {
          title: 'Daftar Kelas',
          route: 'teacher-classroom',
        },
        {
          title: 'Buat Kelas',
          route: 'teacher-classroom-add',
        },
      ],
    },
  ],
  student: [
    {
      header: 'Akses Menu',
    },
    {
      title: 'Dashboard',
      icon: 'ti-home',
      route: 'student-dashboard',
    },
    {
      title: 'Jadwal',
      icon: 'ti-calendar-event',
      route: 'student-schedule',
    },
    {
      title: 'Kelas',
      icon: 'ti-school',
      route: 'student-classroom',
    },
    {
      title: 'Ujian',
      icon: 'ti-file-text',
      route: 'student-exam',
    },
    {
      title: 'Hasil Belajar',
      icon: 'ti-stars',
      route: 'student-report',
    },
  ],
}
